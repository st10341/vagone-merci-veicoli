using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    public class ClsTreno
    {
        #region Variabili
        //<-
        private ClsVagone _vagone; //Pila di vagoni
        //->
        #endregion

        #region Propiretà
        //<-

        /// <summary>
        /// Restituisce la coda di vagoni
        /// </summary>
        public ClsVagone Vagone
        {
            get => _vagone;
        } 
        
        /// <summary>
        /// Restituisce il numero di vagoni attaccati al treno
        /// </summary>
        public int Count
        {
            get
            {
                ClsVagone _temp = this.Vagone;
                int counted = 0;

                while (_temp != null)
                {
                    counted++;
                    _temp = _temp.Next;
                }

                return counted;
            }
        }
        //->
        #endregion

        #region Costruttore
        //<-

        /// <summary>
        /// Costruttore della classe ClsTreno
        /// </summary>
        /// <param name="vagone">OPZIONALE => pila di vagoni gia istanziata</param>
        public ClsTreno(ClsVagone vagone = null)
        {
            this._vagone = vagone;
        }

        //->
        #endregion

        #region Metodi
        //<-

        /// <summary>
        /// Permette di aggiungere un vagone alla pila all'interno del treno
        /// </summary>
        /// <param name="vagone">vagone da aggiungere</param>
        public void Push(ClsVagone vagone)
        {
            //Errore nel valore di veicolo passato alla funzione
            if (vagone == null)
                throw new Exception("Il vagone non può essere di tipo null");

            //Se il treno è vuoto sostituisce direttamente
            if (this._vagone == null)
                this._vagone = vagone;

            //Altrimenti scala di una posizione il vagone gia esistente
            else
            {
                vagone.Next = this._vagone;
                this._vagone = vagone;
            }
        }

        /// <summary>
        /// Rimuovi l'ultimo vagone inserito in pila
        /// </summary>
        /// <returns>L'ultimo vagone inserito in pila</returns>
        public ClsVagone Pop()
        {
            //Variabile d'appoggio
            ClsVagone _toGive;

            //Se c'è almeno un veicolo
            if (this._vagone != null)
            {
                //Restituisce l'ultimo veicolo
                _toGive = this.Vagone;

                //_toGive.Next = null; ---- DA PROBLEMI, prende _toGive per riferimento

                //Scala di un veicolo la pila
                this._vagone = this._vagone.Next;
                return _toGive;
            }
            else //Non c'è alcun veicolo all'interno del vagone
                throw new Exception("La pila di vagoni è vuota");
        }


        /// <summary>
        /// Ricerca di un veicolo all'interno dell'intero treno tramite targa
        /// </summary>
        /// <param name="targa">Targa del veicolo da ricercare</param>
        /// <param name="indexVagone">OUT => Indice del vagone in cui si trova il veicolo</param>
        /// <param name="indexVeicolo">OUT => Indice del veicolo all'interno del vagone</param>
        /// <returns>True se l'operazione di ricerca è andata a buon fine, altrimenti False</returns>
        public bool RicercaVeicolo(string targa, out int indexVagone, out int indexVeicolo)
        {
            //Se non esiste alcun vagone all'interno del treno è inutile continuare la ricerca
            if (this.Count == 0)
            {
                indexVagone = -1;
                indexVeicolo = -1;
                return false;
            }

            //Altrimenti se c'è almeno un vagone...

            //Variabile d'appoggio del vagone
            ClsVagone _temp = this.Vagone;

            //Segnaposto
            int _indexVagone = 0;

            while (_temp != null)
            {
                //Sfrutta la funzione di ricercaVeicolo all'interno dell'oggetto vagone per verificare che ci sia un veicolo al suo interno
                int _indexVeicolo = _temp.RicercaVeicolo(targa);

                //Se il veicolo è stato trovato all'interno del vagone
                if (_indexVeicolo != -1)
                {
                    indexVagone = _indexVagone;
                    indexVeicolo = _indexVeicolo;
                    return true;
                }

                //Altrimenti
                _indexVagone++;
                _temp = _temp.Next;
            }

            //Se non è stato trovato alcun veicolo con targa corrispondente all'interno del treno
            indexVagone = -1;
            indexVeicolo = -1;
            return false;
        }

        /// <summary>
        /// Restituisce le informazioni di un vagone situato in una certa posizione
        /// </summary>
        /// <param name="posizione">Posizione del vaogne nel veicolo (0 = ultimo vagone inserito, 1 = penultimo ...)</param>
        /// <returns>Vagone ricercato tramite indice</returns>
        public ClsVagone GetByIndex(int posizione)
        {
            //Controlla la posizione data sia accettabile
            if ((posizione + 1) > this.Count || posizione < 0)
                throw new Exception("Indice al di fuori dei limiti");

            //Variabile d'appoggio
            ClsVagone _toGive = this.Vagone;

            //Scorre ogni vagone fino ad arrivare al richiesto
            for (int i = 0; i < posizione; i++)
                _toGive = _toGive.Next;

            //Ritorna il vagone ricercato
            return _toGive;
        }

        /*
        --LA FUNZIONE E' DISABILITATA

        /// <summary>
        /// Restituisce la pila di vagoni in formato Lista<ClsVagone>
        /// </summary>
        /// <returns></returns>
        public List<ClsVagone> GetWagonList()
        {
            //Lista da restituire
            List<ClsVagone> _toGive = new List<ClsVagone>();

            //Variabile d'appoggio
            ClsVagone appoggio = this.Vagone;

            for (int i = 0; i < this.Count; i++)
            {
                //Aggiungi il vagone alla lista
                _toGive.Add(appoggio);

                //Pulisci la variabile Next del vagone
                _toGive[i].Next = null;

                //Punta al prox vagone
                appoggio = appoggio.Next;
            }

            //Ritorna la Pila in formato Lista
            return _toGive;
        }

        */

        //->
        #endregion 
    }
}
