﻿namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    partial class FrmVeicolo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTarga = new System.Windows.Forms.TextBox();
            this.tbAnnotazioni = new System.Windows.Forms.TextBox();
            this.tbCF = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTitolo = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btApplica = new System.Windows.Forms.Button();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.nudLunghezzaVeicolo = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLunghezzaVeicolo)).BeginInit();
            this.SuspendLayout();
            // 
            // tbTarga
            // 
            this.tbTarga.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTarga.Location = new System.Drawing.Point(18, 92);
            this.tbTarga.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbTarga.Name = "tbTarga";
            this.tbTarga.Size = new System.Drawing.Size(279, 27);
            this.tbTarga.TabIndex = 0;
            // 
            // tbAnnotazioni
            // 
            this.tbAnnotazioni.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnnotazioni.Location = new System.Drawing.Point(18, 266);
            this.tbAnnotazioni.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbAnnotazioni.Multiline = true;
            this.tbAnnotazioni.Name = "tbAnnotazioni";
            this.tbAnnotazioni.Size = new System.Drawing.Size(279, 68);
            this.tbAnnotazioni.TabIndex = 1;
            // 
            // tbCF
            // 
            this.tbCF.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCF.Location = new System.Drawing.Point(18, 208);
            this.tbCF.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbCF.Name = "tbCF";
            this.tbCF.Size = new System.Drawing.Size(279, 27);
            this.tbCF.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 185);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Codice Fiscale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Targa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 127);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(253, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Lunghezza del veicolo (metri)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 243);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Annotazioni";
            // 
            // lblTitolo
            // 
            this.lblTitolo.AutoSize = true;
            this.lblTitolo.Font = new System.Drawing.Font("MS Reference Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitolo.Location = new System.Drawing.Point(12, 9);
            this.lblTitolo.Name = "lblTitolo";
            this.lblTitolo.Size = new System.Drawing.Size(285, 26);
            this.lblTitolo.TabIndex = 8;
            this.lblTitolo.Text = "INSERIMENTO VEICOLO";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EsLab_VagoneVeicoli_DelloPreite_Filonzi.Properties.Resources.lineaOrizzontale;
            this.pictureBox1.Location = new System.Drawing.Point(18, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(279, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // btApplica
            // 
            this.btApplica.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btApplica.Location = new System.Drawing.Point(203, 341);
            this.btApplica.Margin = new System.Windows.Forms.Padding(4);
            this.btApplica.Name = "btApplica";
            this.btApplica.Size = new System.Drawing.Size(94, 39);
            this.btApplica.TabIndex = 11;
            this.btApplica.Text = "Applica";
            this.btApplica.UseVisualStyleBackColor = true;
            this.btApplica.Click += new System.EventHandler(this.btApplica_Click);
            // 
            // btAnnulla
            // 
            this.btAnnulla.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btAnnulla.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAnnulla.Location = new System.Drawing.Point(18, 341);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(4);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(100, 39);
            this.btAnnulla.TabIndex = 12;
            this.btAnnulla.Text = "Annulla";
            this.btAnnulla.UseVisualStyleBackColor = true;
            // 
            // nudLunghezzaVeicolo
            // 
            this.nudLunghezzaVeicolo.DecimalPlaces = 3;
            this.nudLunghezzaVeicolo.Location = new System.Drawing.Point(18, 150);
            this.nudLunghezzaVeicolo.Name = "nudLunghezzaVeicolo";
            this.nudLunghezzaVeicolo.Size = new System.Drawing.Size(279, 23);
            this.nudLunghezzaVeicolo.TabIndex = 13;
            // 
            // FrmVeicolo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 392);
            this.Controls.Add(this.nudLunghezzaVeicolo);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.btApplica);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblTitolo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCF);
            this.Controls.Add(this.tbAnnotazioni);
            this.Controls.Add(this.tbTarga);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmVeicolo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VEICOLO";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLunghezzaVeicolo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTarga;
        private System.Windows.Forms.TextBox tbAnnotazioni;
        private System.Windows.Forms.TextBox tbCF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTitolo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btApplica;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.NumericUpDown nudLunghezzaVeicolo;
    }
}