﻿namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    partial class FrmInserimentoVagoni
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_vagoni = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bt_NuovoVagone = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bt_EliminaVagone = new System.Windows.Forms.Button();
            this.bt_modificaVagone = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lv_vagoni
            // 
            this.lv_vagoni.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lv_vagoni.FullRowSelect = true;
            this.lv_vagoni.HideSelection = false;
            this.lv_vagoni.Location = new System.Drawing.Point(19, 75);
            this.lv_vagoni.Margin = new System.Windows.Forms.Padding(4);
            this.lv_vagoni.MultiSelect = false;
            this.lv_vagoni.Name = "lv_vagoni";
            this.lv_vagoni.Size = new System.Drawing.Size(316, 264);
            this.lv_vagoni.TabIndex = 0;
            this.lv_vagoni.UseCompatibleStateImageBehavior = false;
            this.lv_vagoni.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "VAGONE";
            this.columnHeader1.Width = 113;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "AUTO PRESENTI";
            this.columnHeader2.Width = 143;
            // 
            // bt_NuovoVagone
            // 
            this.bt_NuovoVagone.Location = new System.Drawing.Point(19, 347);
            this.bt_NuovoVagone.Margin = new System.Windows.Forms.Padding(4);
            this.bt_NuovoVagone.Name = "bt_NuovoVagone";
            this.bt_NuovoVagone.Size = new System.Drawing.Size(100, 51);
            this.bt_NuovoVagone.TabIndex = 1;
            this.bt_NuovoVagone.Text = "Nuovo vagone";
            this.bt_NuovoVagone.UseVisualStyleBackColor = true;
            this.bt_NuovoVagone.Click += new System.EventHandler(this.bt_NuovoVagone_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 34);
            this.label1.TabIndex = 3;
            this.label1.Text = "VAGONI";
            // 
            // bt_EliminaVagone
            // 
            this.bt_EliminaVagone.Location = new System.Drawing.Point(127, 347);
            this.bt_EliminaVagone.Margin = new System.Windows.Forms.Padding(4);
            this.bt_EliminaVagone.Name = "bt_EliminaVagone";
            this.bt_EliminaVagone.Size = new System.Drawing.Size(100, 51);
            this.bt_EliminaVagone.TabIndex = 6;
            this.bt_EliminaVagone.Text = "Elimina vagone";
            this.bt_EliminaVagone.UseVisualStyleBackColor = true;
            this.bt_EliminaVagone.Click += new System.EventHandler(this.bt_EliminaVagone_Click);
            // 
            // bt_modificaVagone
            // 
            this.bt_modificaVagone.Location = new System.Drawing.Point(235, 347);
            this.bt_modificaVagone.Margin = new System.Windows.Forms.Padding(4);
            this.bt_modificaVagone.Name = "bt_modificaVagone";
            this.bt_modificaVagone.Size = new System.Drawing.Size(100, 51);
            this.bt_modificaVagone.TabIndex = 7;
            this.bt_modificaVagone.Text = "Visualizza vagone";
            this.bt_modificaVagone.UseVisualStyleBackColor = true;
            this.bt_modificaVagone.Click += new System.EventHandler(this.bt_modificaVagone_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::EsLab_VagoneVeicoli_DelloPreite_Filonzi.Properties.Resources.lineaOrizzontale;
            this.pictureBox1.Location = new System.Drawing.Point(19, 47);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(316, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // FrmInserimentoVagoni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 411);
            this.Controls.Add(this.bt_modificaVagone);
            this.Controls.Add(this.bt_EliminaVagone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bt_NuovoVagone);
            this.Controls.Add(this.lv_vagoni);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmInserimentoVagoni";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "INSERIMENTO VAGONI";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_vagoni;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button bt_NuovoVagone;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bt_EliminaVagone;
        private System.Windows.Forms.Button bt_modificaVagone;
    }
}

