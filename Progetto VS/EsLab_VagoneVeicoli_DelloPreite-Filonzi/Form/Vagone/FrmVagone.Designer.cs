﻿namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    partial class FrmVagone
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNomeVagone = new System.Windows.Forms.Label();
            this.btEliminaVeicolo = new System.Windows.Forms.Button();
            this.bt_modificaVeicolo = new System.Windows.Forms.Button();
            this.bt_nuovoVeicolo = new System.Windows.Forms.Button();
            this.lv_veicoli = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bt_dettagliVeicolo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNomeVagone
            // 
            this.lblNomeVagone.AutoSize = true;
            this.lblNomeVagone.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeVagone.Location = new System.Drawing.Point(12, 9);
            this.lblNomeVagone.Name = "lblNomeVagone";
            this.lblNomeVagone.Size = new System.Drawing.Size(94, 20);
            this.lblNomeVagone.TabIndex = 0;
            this.lblNomeVagone.Text = "VAGONE n";
            // 
            // btEliminaVeicolo
            // 
            this.btEliminaVeicolo.Location = new System.Drawing.Point(108, 263);
            this.btEliminaVeicolo.Margin = new System.Windows.Forms.Padding(4);
            this.btEliminaVeicolo.Name = "btEliminaVeicolo";
            this.btEliminaVeicolo.Size = new System.Drawing.Size(84, 51);
            this.btEliminaVeicolo.TabIndex = 6;
            this.btEliminaVeicolo.Text = "Elimina veicolo";
            this.btEliminaVeicolo.UseVisualStyleBackColor = true;
            this.btEliminaVeicolo.Click += new System.EventHandler(this.btEliminaVeicolo_Click);
            // 
            // bt_modificaVeicolo
            // 
            this.bt_modificaVeicolo.Location = new System.Drawing.Point(200, 263);
            this.bt_modificaVeicolo.Margin = new System.Windows.Forms.Padding(4);
            this.bt_modificaVeicolo.Name = "bt_modificaVeicolo";
            this.bt_modificaVeicolo.Size = new System.Drawing.Size(84, 51);
            this.bt_modificaVeicolo.TabIndex = 7;
            this.bt_modificaVeicolo.Text = "Modifica veicolo";
            this.bt_modificaVeicolo.UseVisualStyleBackColor = true;
            this.bt_modificaVeicolo.Click += new System.EventHandler(this.bt_modificaVeicolo_Click);
            // 
            // bt_nuovoVeicolo
            // 
            this.bt_nuovoVeicolo.Location = new System.Drawing.Point(16, 263);
            this.bt_nuovoVeicolo.Margin = new System.Windows.Forms.Padding(4);
            this.bt_nuovoVeicolo.Name = "bt_nuovoVeicolo";
            this.bt_nuovoVeicolo.Size = new System.Drawing.Size(84, 51);
            this.bt_nuovoVeicolo.TabIndex = 8;
            this.bt_nuovoVeicolo.Text = "Nuovo veicolo";
            this.bt_nuovoVeicolo.UseVisualStyleBackColor = true;
            this.bt_nuovoVeicolo.Click += new System.EventHandler(this.bt_nuovoVeicolo_Click);
            // 
            // lv_veicoli
            // 
            this.lv_veicoli.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lv_veicoli.FullRowSelect = true;
            this.lv_veicoli.HideSelection = false;
            this.lv_veicoli.Location = new System.Drawing.Point(16, 32);
            this.lv_veicoli.Name = "lv_veicoli";
            this.lv_veicoli.Size = new System.Drawing.Size(360, 224);
            this.lv_veicoli.TabIndex = 9;
            this.lv_veicoli.UseCompatibleStateImageBehavior = false;
            this.lv_veicoli.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "TARGA";
            this.columnHeader1.Width = 90;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "CF PROPRIETARIO";
            this.columnHeader2.Width = 233;
            // 
            // bt_dettagliVeicolo
            // 
            this.bt_dettagliVeicolo.Location = new System.Drawing.Point(292, 263);
            this.bt_dettagliVeicolo.Margin = new System.Windows.Forms.Padding(4);
            this.bt_dettagliVeicolo.Name = "bt_dettagliVeicolo";
            this.bt_dettagliVeicolo.Size = new System.Drawing.Size(84, 51);
            this.bt_dettagliVeicolo.TabIndex = 10;
            this.bt_dettagliVeicolo.Text = "Dettagli veicolo";
            this.bt_dettagliVeicolo.UseVisualStyleBackColor = true;
            this.bt_dettagliVeicolo.Click += new System.EventHandler(this.bt_dettagliVeicolo_Click);
            // 
            // FrmVagone
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 328);
            this.Controls.Add(this.bt_dettagliVeicolo);
            this.Controls.Add(this.lv_veicoli);
            this.Controls.Add(this.bt_nuovoVeicolo);
            this.Controls.Add(this.bt_modificaVeicolo);
            this.Controls.Add(this.btEliminaVeicolo);
            this.Controls.Add(this.lblNomeVagone);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmVagone";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DETTAGLI VAGONE";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomeVagone;
        private System.Windows.Forms.Button btEliminaVeicolo;
        private System.Windows.Forms.Button bt_modificaVeicolo;
        private System.Windows.Forms.Button bt_nuovoVeicolo;
        private System.Windows.Forms.ListView lv_veicoli;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button bt_dettagliVeicolo;
    }
}