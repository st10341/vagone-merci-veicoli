﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    public partial class FrmVeicolo : Form
    {
        public ClsVeicolo Veicolo = null;
        public enum eModalita
        {
            Aggiungi,
            Modifica,
            Dettagli
        }

        public FrmVeicolo(eModalita modalita, string targa)
        {
            InitializeComponent();


            if (modalita != eModalita.Aggiungi)
            {
                int _indexVagone, _indexVeicolo;

                bool founded = Program.Treno.RicercaVeicolo(targa, out _indexVagone, out _indexVeicolo);

                if (!founded)
                {
                    MessageBox.Show("Non sono riuscito a trovare il veicolo ricercato");
                    this.Close();
                }
                else
                    Veicolo = Program.Treno.GetByIndex(_indexVagone).GetByIndex(_indexVeicolo);


                if (modalita == eModalita.Modifica)
                {
                    CompilaCampi();
                }
                else if (modalita == eModalita.Dettagli)
                {
                    DisattivaCampi();
                    CompilaCampi();
                }
            }
        }


        #region EVENTI
        private void btApplica_Click(object sender, EventArgs e)
        {
            try
            {
                if (Veicolo == null)
                    Veicolo = new ClsVeicolo(tbTarga.Text, tbCF.Text, tbAnnotazioni.Text, nudLunghezzaVeicolo.Value);
                else
                {
                    Veicolo.Targa = tbTarga.Text;
                    Veicolo.CFproprietario = tbCF.Text;
                    Veicolo.Annotazioni = tbAnnotazioni.Text;
                    Veicolo.Lunghezza = nudLunghezzaVeicolo.Value;
                }
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
            
        }
        #endregion

        #region METODI
        private void DisattivaCampi()
        {
            tbTarga.Enabled = false;
            tbCF.Enabled = false;
            tbAnnotazioni.Enabled = false;
            nudLunghezzaVeicolo.Enabled = false;
            btApplica.Visible = false;
        }

        private void CompilaCampi()
        {
            tbTarga.Text = Veicolo.Targa;
            tbCF.Text = Veicolo.CFproprietario;
            tbAnnotazioni.Text = Veicolo.Annotazioni;
            nudLunghezzaVeicolo.Value = Veicolo.Lunghezza;
        }

        #endregion
    }
}
