using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    public class ClsVagone
    {
        #region Variabili
        //<-
        private const decimal _capacita = 15;   //Lunghezza massima del totale di veicoli trasportabile all'interno del vagone
        private ClsVeicolo _auto;               //Pila di auto all'interno del vagone
        private ClsVagone _next;                //Prossimo vagone in pila
        //->
        #endregion

        #region Propiretà
        //<-

        /// <summary>
        /// Restituisce la capacità massima in lunghezza dei veicoli
        /// </summary>
        public decimal Capacita
        {
            get => _capacita;
        }


        /// <summary>
        /// Restituisce la coda di auto
        /// </summary>
        public ClsVeicolo Auto
        {
            get => _auto;
        }


        /// <summary>
        /// Restituisce il prossimo vagone in pila
        /// </summary>
        public ClsVagone Next
        {
            get => _next;
            set => _next = value;
        }


        /// <summary>
        /// Restituisce il numero di veicoli presenti nel vagone
        /// </summary>
        public int Count
        {
            get
            {
                ClsVeicolo _temp = this.Auto;
                int counted = 0;

                while (_temp != null)
                {
                    counted++;
                    _temp = _temp.Next;
                }
                
                return counted;
            }
        }

        /// <summary>
        /// Restituisce lo spazio occupato nel vagone in decimal
        /// </summary>
        public decimal CountSpace
        {
            get
            {
                ClsVeicolo _temp = this.Auto;
                decimal occupied = 0;

                while (_temp != null)
                {
                    occupied += _temp.Lunghezza;
                    _temp = _temp.Next;
                }

                return occupied;
            }
        }
        
        //->
        #endregion
        
        #region Costruttore
        //<-

        /// <summary>
        /// Costruttore della classe ClsVagone
        /// </summary>
        /// <param name="pilaVeicoli">OPZIONALE => pila di veicoli gia istanziata</param>
        public ClsVagone(ClsVeicolo pilaVeicoli = null)
        {
            this._auto = pilaVeicoli;
        }

        //->
        #endregion
        
        #region Metodi
        //<-

        /// <summary>
        /// Permette di aggiungere un veicolo alla pila all'interno del vagone
        /// </summary>
        /// <param name="veicolo">Veicolo da aggiungere</param>
        public void Push(ClsVeicolo veicolo)
        {
            //Errore nell'oggetto passato
            if (veicolo == null)
                throw new Exception("Il veicolo non può essere di tipo null");

            //Max capacity exceeded
            if ((this.CountSpace + veicolo.Lunghezza) > this.Capacita)
                throw new Exception($"Il veicolo è troppo grande è stata superata la capacità massima del vagone, (capacità: {this.CountSpace}/{this.Capacita} Metri)");

            //Se il vagone è vuoto sostituisce direttamente
            if (this._auto == null)
                this._auto = veicolo;

            //Altrimenti scala di una posizione il veicolo gia esistente
            else
            {
                veicolo.Next = this._auto;
                this._auto = veicolo;
            }
        }

        /// <summary>
        /// Rimuovi l'ultimo veicolo inserito in pila
        /// </summary>
        /// <returns>L'ultimo veicolo inserito in pila</returns>
        public ClsVeicolo Pop()
        {
            //Variabile d'appoggio
            ClsVeicolo _toGive;

            //Se c'è almeno un veicolo
            if (this._auto != null)
            {
                //Restituisce l'ultimo veicolo
                _toGive = this._auto;

                //_toGive.Next = null; ---- DA PROBLEMI, prende _toGive per riferimento

                //Scala di un veicolo la pila
                this._auto = this._auto.Next;
                return _toGive;
            }
            else //Non c'è alcun veicolo all'interno del vagone
                throw new Exception("La pila di auto è vuota");
        }

        /// <summary>
        /// Ricerca di un veicolo all'interno del vagone tramite targa
        /// </summary>
        /// <param name="targa">Targa del veicolo da ricercare</param>
        /// <returns>Posizione del veicolo all'interno del vagone</returns>
        public int RicercaVeicolo(string targa)
        {
            
            //L'auto sicuramente non è all'interno del vagone se esso non ne contiene alcuna
            if (this.Count == 0)
                return -1;

            //Variabile d'appoggio dell'auto
            ClsVeicolo _temp = this.Auto;

            //Segnaposto
            int _posizione = 0;

            //Controlla ogni auto una ad una finche non arriva a null
            while (_temp != null)
            {
                //Ritorna la posizione nel vagone se la targa coincide
                if (_temp.Targa == targa)
                    return _posizione;

                //Aggiorna la posizione e il segnaposto
                _posizione++;
                _temp = _temp.Next;

            }

            //Se il programma arriva fino a qui l'auto sicuramente non è all'interno del vagone
            return -1;
        }

        /// <summary>
        /// Restituisci le informazioni di un veicolo situato in una certa posizione
        /// </summary>
        /// <param name="posizione">Posizione del veicolo nel vagone (0 = ultimo veicolo inserito, 1 = penultimo ...)</param>
        /// <returns>Veicolo ricercato tramite indice</returns>
        public ClsVeicolo GetByIndex(int posizione)
        {
            //Controlla la posizione data sia accettabile
            if ((posizione + 1) > this.Count || posizione < 0)
                throw new Exception("Indice al di fuori dei limiti");

            //Variabile d'appoggio
            ClsVeicolo _toGive = this.Auto;

            //Scorre ogni veicolo fino ad arrivare al richiesto
            for (int i = 0; i < posizione; i++)
                _toGive = _toGive.Next;

            //Ritorna il veicolo ricercato
            return _toGive;
        }

        /*
        --LA FUNZIONE E' DISABILITATA

        /// <summary>
        /// Restituisce la pila di veicoli in formato Lista<ClsVeicolo>
        /// </summary>
        /// <returns></returns>
        public List<ClsVeicolo> GetVeicleList()
        {
            //Lista da restituire
            List<ClsVeicolo> _toGive = new List<ClsVeicolo>();

            //Variabile d'appoggio
            ClsVeicolo appoggio = this.Auto;

            for (int i = 0; i < this.Count; i++)
            {
                //Aggiungi il veicolo alla lista
                _toGive.Add(appoggio);

                //Pulisci la variabile Next del veicolo
                _toGive[i].Next = null;

                //Punta al prox veicolo
                appoggio = appoggio.Next;
            }

            //Ritorna la Pila in formato Lista
            return _toGive;
        }

        */

        //->
        #endregion
    }
}