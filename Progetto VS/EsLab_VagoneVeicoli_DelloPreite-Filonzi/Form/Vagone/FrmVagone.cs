﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    public partial class FrmVagone : Form
    {
        ClsVagone Vagone;

        public FrmVagone(int indexVagone)
        {
            InitializeComponent();
            Vagone = Program.Treno.GetByIndex(indexVagone);
            refreshListView();

            lblNomeVagone.Text = "Vagone " + (Program.Treno.Count - indexVagone);
        }


        #region Eventi
        //<-

        private void btEliminaVeicolo_Click(object sender, EventArgs e)
        {
            if (Vagone.Count == 0) //Se il vagone è vuoto
            {
                MessageBox.Show("Non sono presenti veicoli nel vagone");
                return;
            }

            //Chiedi conferma
            DialogResult dr = MessageBox.Show("Sei sicuro di volere eliminare l'auto con targa " + Vagone.Auto.Targa + "?", "ATTENZIONE", MessageBoxButtons.YesNo);

            //Se ha rifiutato interrompi
            if (dr != DialogResult.Yes)
                return;

            //Elimina
            Vagone.Pop();
            refreshListView();
        }

        private void bt_nuovoVeicolo_Click(object sender, EventArgs e)
        {
            FrmVeicolo frmVeicolo = new FrmVeicolo(FrmVeicolo.eModalita.Aggiungi, null);
            frmVeicolo.ShowDialog();

            try
            {
                Vagone.Push(frmVeicolo.Veicolo);
            } catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }

            refreshListView();
        }

        private void bt_modificaVeicolo_Click(object sender, EventArgs e)
        {
            if (lv_veicoli.SelectedItems.Count == 0)
                MessageBox.Show("Seleziona un veicolo");
            else
                new FrmVeicolo(FrmVeicolo.eModalita.Modifica, lv_veicoli.SelectedItems[0].Tag.ToString()).ShowDialog();

            refreshListView();
        }

        private void bt_dettagliVeicolo_Click(object sender, EventArgs e)
        {
            if (lv_veicoli.SelectedItems.Count == 0)
                MessageBox.Show("Seleziona un veicolo");
            else
                new FrmVeicolo(FrmVeicolo.eModalita.Dettagli, lv_veicoli.SelectedItems[0].Tag.ToString()).ShowDialog();

        }

        //->
        #endregion

        #region Metodi
        //<-

        private void refreshListView()
        {
            lv_veicoli.Items.Clear();

            int nVeicoli = Vagone.Count;

            for (int i = 0; i < nVeicoli; i++)
            {
                ClsVeicolo _veicolo = Vagone.GetByIndex(i);

                ListViewItem row = new ListViewItem();
                row.Text = _veicolo.Targa;
                row.Tag = _veicolo.Targa;
                row.SubItems.Add(_veicolo.CFproprietario);

                lv_veicoli.Items.Add(row);
            }
        }




        //->
        #endregion


    }
}
