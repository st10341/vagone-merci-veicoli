using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    public class ClsVeicolo
    {
        #region Variabili
        //<-
        string _targa;
        string _cfProprietario;
        string _annotazioni;
        decimal _lunghezza;
        ClsVeicolo _next;
        //->
        #endregion

        #region Propiretà
        //<-
        public string Targa
        {
            get => _targa;
            set
            {
                if (
                    //La lunghezza della targa equivale a la tipica lunghezza
                    value.Length == 7
                    &&
                    //ogni carattere è al posto giusto
                    Char.IsLetter(value[0]) && Char.IsLetter(value[1]) && Char.IsDigit(value[2]) && Char.IsDigit(value[3]) && Char.IsDigit(value[4]) && Char.IsLetter(value[5]) && Char.IsLetter(value[6])
                    )
                    _targa = value;
                else
                    throw new Exception("Formato targa non valido, Utilizzare AB123CD");

            }
        }

        public string CFproprietario
        {
            get => _cfProprietario;
            set
            {
                if (
                    //La lunghezza del codice fiscale equivale a la tipica lunghezza
                    value.Length == 16
                    &&
                    //ogni carattere è al posto giusto
                    Char.IsLetter(value[0]) && Char.IsLetter(value[1]) && Char.IsLetter(value[2]) && Char.IsLetter(value[3]) && Char.IsLetter(value[4]) && Char.IsLetter(value[5]) && Char.IsDigit(value[6]) && Char.IsDigit(value[7]) && Char.IsLetter(value[8]) && Char.IsDigit(value[9]) && Char.IsDigit(value[10]) && Char.IsLetter(value[11]) && Char.IsDigit(value[12]) && Char.IsDigit(value[13]) && Char.IsDigit(value[14]) && Char.IsLetter(value[15])
                    )
                    _cfProprietario = value;
                else
                    throw new Exception("Formato codice fiscale non valido");

            }
        }

        public string Annotazioni
        {
            get => _annotazioni;
            set => _annotazioni = value;
        }

        public decimal Lunghezza
        {
            get => _lunghezza;
            set => _lunghezza = value;
        }
        public ClsVeicolo Next
        {
            get => _next;
            set => _next = value;
        }
        //->
        #endregion
        
        #region Costruttore
        //<-
        public ClsVeicolo(string targa, string cfProprietario, string annotazioni, decimal lunghezza)
        {
            this.Targa = targa;
            this.CFproprietario = cfProprietario;
            this.Annotazioni = annotazioni;
            this.Lunghezza = lunghezza;
        }
        //->
        #endregion
        
        #region Metodi

        #endregion 
    }
}
