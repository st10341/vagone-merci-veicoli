﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsLab_VagoneVeicoli_DelloPreite_Filonzi
{
    public partial class FrmInserimentoVagoni : Form
    {
        public FrmInserimentoVagoni()
        {
            InitializeComponent();
        }

        #region Eventi
        //<-
        private void bt_NuovoVagone_Click(object sender, EventArgs e)
        {
            ClsVagone _vagone = new ClsVagone();
            Program.Treno.Push(_vagone);

            refreshListView();

        }

        private void bt_EliminaVagone_Click(object sender, EventArgs e)
        {
            Program.Treno.Pop();
            refreshListView();
        }


        private void bt_modificaVagone_Click(object sender, EventArgs e)
        {
            if (lv_vagoni.SelectedItems.Count == 0)
                MessageBox.Show("Seleziona un vagone");
            else
                new FrmVagone((int)lv_vagoni.SelectedItems[0].Tag).ShowDialog();

            refreshListView();
        }


        //->
        #endregion

        #region Funzioni
        //<-

        private void refreshListView()
        {
            lv_vagoni.Items.Clear();

            int nVagoni = Program.Treno.Count;

            for (int i = 0; i < nVagoni; i++)
            {
                ClsVagone _vagone = Program.Treno.GetByIndex(i);

                ListViewItem row = new ListViewItem();
                row.Text = $"Vagone {nVagoni - i}";
                row.Tag = i;
                row.SubItems.Add(_vagone.Count.ToString());

                lv_vagoni.Items.Add(row);
            }
        }


        //->
        #endregion

    }
}
